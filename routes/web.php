<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::group([
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => ['Shared']			
], function()
{
	/** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
	Route::get('/', function()
	{
		//return App::getLocale();
		return view('home');
		//$categories = \App\Product_category::where('parent_id', '=', 0)->get();
		//$allCategories = \App\Product_category::with('parent')->with('children')->get()->toArray();
		// $allCategories = \App\Product_category::with('children')->where('parent_id',0)->with('translations')->get()->toArray();
		// $allCategories = \App\Http\Helpers\LangHelper::translate($allCategories,\App::getLocale());
		// //dd($allCategories);
	});

	Route::get('test',function(){
		return View::make('aboutus');
	});
});
