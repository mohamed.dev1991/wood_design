<?php

namespace App;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Translatable;
    protected $table = "products";
    protected $translatable = ['title','body','slug'];

    public function product_category(){
        return $this->belongsTo("App\Product_category")->with('translations');
    }

    public function product_colors(){
        return $this->belongsToMany(\App\Color::class,'product__colors','product_id','color_id')->with('translations');
    }

    
}
