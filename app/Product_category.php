<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;

class Product_category extends Model
{
    use Translatable;
    protected $table = "product_categories";
    protected $translatable = ['title','body','slug','meta_title','meta_desc','meta_keywords'];

   /* public function childs() {
        return $this->hasMany('App\Product_category','parent_id','id') ;
    }*/

    public function parent()
    {
        return $this->belongsTo('App\Product_category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Product_category', 'parent_id')->with('children')->with('translations');
    }
}
