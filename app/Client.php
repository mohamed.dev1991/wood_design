<?php

namespace App;

use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use Translatable;
    protected $translatable = ['title'];
}
