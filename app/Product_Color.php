<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Color extends Model
{
    protected $table = "product__colors";
}
