<?php

namespace App\Dimmers;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use App\Product;

class ProductDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Product::count();
        $string = "Product" ;//"You have ".$count." Product in your database. Click on button below to view all Products.";

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'icon voyager-belt',
            'title'  => "{$count} {$string}",
            'text'   => "You have ".$count." Product in your database. Click on button below to view all Products.",//__('voyager.dimmer.page_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => "View all Products",
                'link' => route('voyager.products.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/03.jpg'),
        ]));
    }
}
